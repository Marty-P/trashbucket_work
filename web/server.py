from flask import Flask
from flask import render_template
from flask import request
import web.database
import hashlib

class WServer:
    def __init__(self):
        self.webServer = Flask(__name__)
        self.webServer.static_folder = 'templates/style'
        self.__debug = False
        self.route()
        self.db = web.database.DataBase()

    def run(self):
        self.webServer.run(debug = self.__debug)

    def route(self):
        @self.webServer.route('/')
        def index():
            return render_template('login.html')

        @self.webServer.route('/registration', methods=['GET', 'POST'])
        def registration():
            if request.method == 'POST':
                return self.webRegistration(request.form['username'], request.form['password'], request.form['email'])
            else:
                return render_template('registration.html')

        @self.webServer.route('/login', methods=['POST'])
        def login():
            return request.form['usr']

    def webRegistration(self, username, password, email):
        password = hashlib.sha224(password.encode('utf-8')).hexdigest()
        query = "INSERT INTO users (name, password, email) VALUES ('" + str(username) +"','" + str(password) + "','" + str(email) + "')"
        self.db.execute(query)
        return 'done'