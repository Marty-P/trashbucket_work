import sqlite3

class DataBase:
    def __init__(self):
        self.connection = sqlite3.connect('TrashBase.db3')
        print('db_init')

    def execute(self, query):
        cursor = self.connection.cursor()
        cursor.execute(query)
        self.connection.commit()
        return cursor.fetchone()

    def __del__(self):
        self.connection.close()
        print('db_del')
