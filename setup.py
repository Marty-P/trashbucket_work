import web.database
import os


db = web.database.DataBase()

query = """CREATE TABLE 'users' ('id' INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                                    'name' 'VARCHAR (30)' UNIQUE NOT NULL,
                                    'email' 'VARCHAR (200)' UNIQUE NOT NULL,
                                    'password' 'CHAR (64)' NOT NULL,
                                    'active' INTEGER DEFAULT '1')"""

db.execute(query)